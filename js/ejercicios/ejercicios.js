$(document).ready( function () {

	/**
	 * VARIABLES
	 */
	
	var nivel;
	var enfoque;
	var div_ejercicios = $('#contenido_ejercicios');

	/**
	 * CARGA DE CONTENIDO ESENCIAL
	 */
	cargarCategoriaEnfoque();

	cargarCategoriaNivel();

	mostrarTodosLosEjercicios();

	/**
	 * ACCIONES DE LOS CONTENIDOS
	 */
	$('#busqueda_ejercicios').keyup(function (){
		var busqueda = $(this).val();
		var titulos_ejercicios = $('.card-title');
		if (busqueda != ''){
			$(".cont_eje").hide();
			$(titulos_ejercicios).each(function (){
				let obj = $(this);
				let texto = $(this).text();
				if (texto.indexOf(busqueda) != -1) {
					let e = obj.parents();
					$(e[1]).show();
				}
			})
		}else
			$(".cont_eje").show();
	})

});



/**
 * [mostrarTodosLosEjercicios muestra todos las imágenes de los ejercicios registrados en la base de datos]
 * @return {[type]} [description]
 */
function mostrarTodosLosEjercicios(){
	var datos = {
		funcion: 'obtenerTodosEjercicios'
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			console.log(respuesta);
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', respuesta['COD_ERR']+' '+respuesta['ERR_DRI']+' '+respuesta['ERR_MSG']);
			else{
				respuesta.forEach(function (e){
					enf = obtenerNombreEnfoque(e[3]);
					niv = obtenerNombreNivel(e[4]);
					lug = obtenerNombreLugar(e[5]);
					$('#contenido_ejercicios').append(`<div class="card cont_eje col-sm-6 col-md-4 col-lg-3 my-2">
					<img class="card-img-top" src="imagenes/ejercicios/`+e[2]+`.png" alt="`+e[1]+`">
						<div class="card-body">
							<h4 class="card-title text-center">`+e[1]+`</h4>
							<p class="enf">Enfoque: `+enf+`</p>
							<p class="niv">Nivel: `+niv+`</p>
							<p class="lug">Lugar: `+lug+`</p>
						</div>
					</div>`)
				});
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', 'Error al cargar los datos de los ejercicios: '+errorThrown);
		}
	});
}

/**
 * [obtenerNombreLugar traduce la clave del lugar a texto legible (para leer)]
 * @param  {[type]} $clave [tipo de lugar a traducir]
 * @return {[type]}        [texto legible del lugar]
 */
function obtenerNombreLugar($clave){
	switch($clave){
		case 'HOG':
			return 'Hogar';
		case 'GEN':
			return 'General';
	}
}

/**
 * [obtenerNombreNivel traduce la clave del nivel a texto legible (para leer)]
 * @param  {[type]} $clave [tipo de nivel a traducir]
 * @return {[type]}        [texto legible del nivel]
 */
function obtenerNombreNivel($clave){
	switch($clave){
		case 'DIF':
			return 'Difícil';
		case 'INTER':
			return 'Intermedio';
		case 'PRIN':
			return 'Principiante';
	}
}

/**
 * [obtenerNombreEnfoque traduce la clave del enfoque a texto legible (para leer)]
 * @param  {[text]} $clave [tipo de enfoque a traducir]
 * @return {[type]}        [texto legible del enfoque]
 */
function obtenerNombreEnfoque($clave){
	switch($clave){
		case 'ABNU':
			return 'Abdómen y núcleo';
		case 'CARD':
			return 'Cardio';
		case 'CENT':
			return 'Cuerpo entero';
		case 'FLEX':
			return 'Flexibilidad';
		case 'PINF':
			return 'Parte inferior del cuerpo';
		case 'PSUP':
			return 'Parte superior';
	}
}

/**
 * [cargarCategoriaNivel description]
 * @return {[type]} [description]
 */
function cargarCategoriaNivel(){
	var datos = {
		funcion: 'obtenerNivel'
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', respuesta['COD_ERR']+' '+respuesta['ERR_DRI']+' '+respuesta['ERR_MSG']);
			else{
				respuesta.forEach(function (e){
					$('#categoria_nivel').append('<option class="dropdown-item" value="'+e[0]+'">'+e[1]+'</option>')
				});
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', 'Error al cargar los datos de los enfoques: '+errorThrown);
		}
	});
}

/**
 * [cargarCategoriaEnfoque Obtiene los valores de enfoque de los ejercicios]
 * @return {nada} [Despliega los valores en el dropdown]
 */
function cargarCategoriaEnfoque(){
	var datos = {
		funcion: 'obtenerEnfoque'
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', respuesta['COD_ERR']+' '+respuesta['ERR_DRI']+' '+respuesta['ERR_MSG']);
			else{
				respuesta.forEach(function (e){
					$('#categoria_enfoque').append('<option class="dropdown-item" value="'+e[0]+'">'+e[1]+'</option>')
				});
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', 'Error al cargar los datos de los enfoques: '+errorThrown);
		}
	});
}

/**
 * [mostrarModalAlerta mostrar modal de acción exitosa]
 * @param  {[text} tipo  [tipo de mensaje: error, info, exito]
 * @param  {[text} texto [mensaje que será desplegado]
 */
function mostrarModalAlerta(tipo, texto){
	$('#texto_mensaje_'+tipo).append(texto);
	$("#alerta_"+tipo).modal("show");
}

/**
 * [limpiarContenido vacia el contenido de un objeto del DOM por id]
 * @param  {[text]} id [id del objeto]
 */
function limpiarContenido(id){
	$('#'+id).empty();
}

/**
 * [cerrarModal cierra un modal, identificado por id]
 * @param  {[text]} id [id del modal a cerrar]
 */
function cerrarModal(id){
	$('#'+id).modal('hide');
}

/**
 * [mostrarModal muestra un modal, identificado por id]
 * @param  {[ttext]} id [id del modal a mostrar]
 */
function mostrarModal(id){
	$('#'+id).modal('show');
}

/**
 * [deshabilitarElementoID deshabilita un elemento por id]
 * @param  {[text]} id [id del elemento a deshabilitat]
 */
function deshabilitarElementoID(id){
	$("#"+id).attr('disabled', '');
}

/**
 * [habilitarElementoID habilita un elemento por id]
 * @param  {[text]} id [id del elemento a habilitar]
 */
function habilitarElementoID(id){
	$("#"+id).removeAttr('disabled', '');
}