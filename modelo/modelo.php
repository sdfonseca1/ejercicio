<?php
/* AUTOR:
*  FECHA DE CREACIÓN:
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN:
*  ANOTACIONES:
*/
/*	El modelo es el encargado de recibir las solicitudes del controlador y no deberá validar datos.
**	Parte de las buenas prácticas de programación, es muy recomendable agregar las siguientes líneas de código después
**	de que se declare una función:
**		UTILIDAD:
**		PRECONDICION: 
**		POSTCONDICIÓN:
**	UTILIDAD consiste en agregar una breve descripción de la acción que realiza tu función.
**	PRECONDICION deberá de llevar un texto narrativo de qué requerimientos necesita tu función para poder
**	trabajar.
**	POSTCONDICION será una breve descripción de cuál será el resultado una vez que la función se ejecute.
**	Adicionalmente se puede agregar debajo de POSTCONDICION, los modificadores de acceso, descripción de los parámetros
**	que son recebidos, entre otros. La anotación adecuada será la siguiente:
**		@private 	DESCRIPCION
**		@public 	DESCRIPCION
**		@protected	DESCRIPCION
**		@param		DESCRIPCION
**	No se obliga al usuario a utilizar lo anteriormente mencionado y si quiere cambiarlo, tiene toda la libertad de caambiarlo
**		
*/
	/*REQUERIMIENTOS DE ARCHIVOS*/
	require_once("conexion.php");

	class Modelo extends Conexion{
		
		/*VARIABLES Y CONSTANTES*/

		/**
		 * [obtenerEnfoqueModelo obtiene los valores de la tabla ENFOQUE]
		 * @param  [text] $tabla [nombre de la tabla para ejecutar la sentencia]
		 * @return [object] [regresa los valores de la tabla ENFOQUE o el código de error]
		 */
		public function obtenerEnfoqueModelo($tabla){
			$sentencia = Conexion::conectar()->prepare("SELECT * FROM $tabla");
			if ($sentencia->execute())
				return $sentencia->fetchAll();
			else
				return obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [obtenerNivelModelo obtiene los valores de la tabla NIVEL]
		 * @param  [type] $tabla [nombre de la tabla para ejecutar la sentencia]
		 * @return [object] [regresa los valores de la tabla NIVEL o el código de error]
		 */
		public function obtenerNivelModelo($tabla){
			$sentencia = Conexion::conectar()->prepare("SELECT * FROM $tabla");
			if ($sentencia->execute())
				return $sentencia->fetchAll();
			else
				return obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [obtenerTodosEjerciciosModelo obtiene todos los ejercicios en la base de datos]
		 * @param  [text] $tabla [nombre de la tabla para ejecutar la sentencia]
		 * @return [object] [regresa los valores de la tabla EJERCICIOS o el código de error]
		 */
		public function obtenerTodosEjerciciosModelo($tabla){
			$sentencia = Conexion::conectar()->prepare("SELECT * FROM $tabla");
			if ($sentencia->execute())
				return $sentencia->fetchAll();
			else
				return obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [obtenerErrorConsulta crear arreglo asociativo con el arreglo numérico de entrada]
		 * @param  [array] $arreglo [arreglo de errores de mysql]
		 * @return [array]          [arreglo asociativo generado con el arreglo de entrada]
		 */
		private function obtenerErrorConsulta($arreglo){
			$asociativo = array('COD_ERR', 'ERR_DRI', 'ERR_MSG');
			return array_combine($asociativo, $arreglo);
		}




	}

?>
