<?php
/* AUTOR:
*  FECHA DE CREACIÓN:
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN:
*  ANOTACIONES:
*/
/*	Encargado de recoger los valores en la barra de dirección para rederigir entre las distintas vistas.
**	Parte de las buenas prácticas de programación, es muy recomendable agregar las siguientes líneas de código después
**	de que se declare una función:
**		UTILIDAD:
**		PRECONDICION: 
**		POSTCONDICIÓN:
**	UTILIDAD consiste en agregar una breve descripción de la acción que realiza tu función.
**	PRECONDICION deberá de llevar un texto narrativo de qué requerimientos necesita tu función para poder
**	trabajar.
**	POSTCONDICION será una breve descripción de cuál será el resultado una vez que la función se ejecute.
**	Adicionalmente se puede agregar debajo de POSTCONDICION, los modificadores de acceso, descripción de los parámetros
**	que son recebidos, entre otros. La anotación adecuada será la siguiente:
**		@private 	DESCRIPCION
**		@public 	DESCRIPCION
**		@protected	DESCRIPCION
**		@param		DESCRIPCION
**	No se obliga al usuario a utilizar lo anteriormente mencionado y si quiere cambiarlo, tiene toda la libertad de caambiarlo
**		
*/
/*VARIABLES Y CONSTANTES*/

	class Paginas{

		/**
		 * [enlacesPaginasModelo description]
		 * @param  text $enlacesModelo valor del accion de una dirección.
		 */
		public function enlacesPaginasModelo($enlacesModelo){
			/*UTILIDAD: lista blanca de valores permitidos en la dirección.
			  PRECONDICION: recibe el nuevo valor de la variale accion de la dirección.
			  POSTCONDICIÓN: muestra la página a la que se quería acceder.
			*/
			 /*Codigo de ejemplo*/
			if($enlacesModelo == "ejercicios")
			   	$modulo = "vista/modulos/ejercicios/inicio_ejercicios.php";
			return $modulo;
			// else if ($enlacesModelo == "alumnos") {
			// 	$modulo = "vista/modulos/archivo.php";
			// }else{
			// 	$modulo = "vista/modulos/archivo.php"
			// }
		}
	}
?>	