<?php
/* AUTOR:
*  FECHA DE CREACIÓN:
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN:
*  ANOTACIONES:
*/
/*	Archivo de conexión a la base de datos, configurable.
**	Parte de las buenas prácticas de programación, es muy recomendable agregar las siguientes líneas de código después
**	de que se declare una función:
**		UTILIDAD:
**		PRECONDICION: 
**		POSTCONDICIÓN:
**	UTILIDAD consiste en agregar una breve descripción de la acción que realiza tu función.
**	PRECONDICION deberá de llevar un texto narrativo de qué requerimientos necesita tu función para poder
**	trabajar.
**	POSTCONDICION será una breve descripción de cuál será el resultado una vez que la función se ejecute.
**	Adicionalmente se puede agregar debajo de POSTCONDICION, los modificadores de acceso, descripción de los parámetros
**	que son recebidos, entre otros. La anotación adecuada será la siguiente:
**		@private 	DESCRIPCION
**		@public 	DESCRIPCION
**		@protected	DESCRIPCION
**		@param		DESCRIPCION
**	No se obliga al usuario a utilizar lo anteriormente mencionado y si quiere cambiarlo, tiene toda la libertad de caambiarlo
**		
*/
	class Conexion{

		/*VARIABLES Y CONSTANTES*/

		//@var string 	nombre de usuario para la base de datos.
		protected $usuario = "root";
		//@var string 	contraseña para la base de datos.
		protected $contrasenha = "SDF10ico-@";
		//@var string 	nombre de la base de datos.
		protected $base_datos = "";
		//@var string 	nombre del servidor de la base de datos.
		protected $servidor = "";

		public function conectar(){
			/*UTILIDAD: realiza la conexión a la base de datos.
			  PRECONDICION: se debe definir los valores de las variables para la conexion.
			  POSTCONDICIÓN: la conexión se realiza exitosamente o muestra mensaje de error.
			*/
			try{
				$link = new PDO("mysql:host=localhost;dbname=ejercitate",'root','SDF10ico-@');
				return $link;
			}catch(PDOException $e){
			 	echo "Error: " . $e->getMessage();
			}
		}
	}
?>