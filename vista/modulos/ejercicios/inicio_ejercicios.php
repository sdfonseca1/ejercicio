<script>
	var url = "js/ejercicios/ejercicios.js";
	$.getScript(url);
</script>
<div class="container">
	<p class="display-4 text-center">Ejercicios</p>
	<div class="card">
		<div class="card-body">
			<p class="text-center text-white">Mostrar los ejercicios por:</p>
			<div class="container row">
				<div class="col-lg-6">
					<span class="offset-6 text-white">Enfoque</span>
					<select class="custom-select dropdown bg-secondary text-white" id="categoria_enfoque">
                        <option selected value="TOD">Todos</option>
                    </select>
				</div>
				<div class="col-lg-6">
					<span class="offset-6 text-white">Nivel</span>
					<select class="custom-select dropdown bg-secondary text-white" id="categoria_nivel">
                        <option selected value="TOD">Todos</option>
                    </select>
				</div>
			</div>
			
  		</div>
	</div>
</div>
<div class="container my-3">
	<h3 class="text-center">Lista de ejercicios</h3>
	<div id="filtro_busqueda" class="my-2 text-center bg-dark">
		<span>Búsqueda</span>
		<input id="busqueda_ejercicios" type="text">
	</div>
	<div id="contenido_ejercicios" class="container row my-2"></div>
</div>