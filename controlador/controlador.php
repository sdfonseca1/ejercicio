<?php
/* AUTOR:
*  FECHA DE CREACIÓN:
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN:
*  ANOTACIONES:
*/

/*	El controlador es el encargado de recibir las solicitudes de las vistas y en caso de que lo requiera, deberá validar
**	los datos que provengan de dichas vistas.
**	Parte de las buenas prácticas de programación, es muy recomendable agregar las siguientes líneas de código después
**	de que se declare una función:
**		UTILIDAD:
**		PRECONDICION: 
**		POSTCONDICIÓN:
**	UTILIDAD consiste en agregar una breve descripción de la acción que realiza tu función.
**	PRECONDICION deberá de llevar un texto narrativo de qué requerimientos necesita tu función para poder
**	trabajar.
**	POSTCONDICION será una breve descripción de cuál será el resultado una vez que la función se ejecute.
**	Adicionalmente se puede agregar debajo de POSTCONDICION, los modificadores de acceso, descripción de los parámetros
**	que son recebidos, entre otros. La anotación adecuada será la siguiente:
**		@private 	DESCRIPCION
**		@public 	DESCRIPCION
**		@protected	DESCRIPCION
**		@param		DESCRIPCION
**	No se obliga al usuario a utilizar lo anteriormente mencionado y si quiere cambiarlo, tiene toda la libertad de caambiarlo
**		
*/
/*REQUERIMIENTOS DE ARCHIVOS*/
// require_once "modelo/modelo.php";

	class Controlador{

		/*VARIABLES Y CONSTANTES*/
		
		/**
		 * [Encargado de cargar la página inicial.]
		 */
		public function pagina(){
			include("vista/plantilla.php");
		}

		/**
		 * [Obtiene el valor del accion en la barra de direcicones.]
		 */
		public function enlacesPaginasControlador(){
			if(isset($_GET['accion'])){	
				$enlaces = $_GET['accion'];
			}else{
				$enlaces = 'index=accion=inicio';
			}
			$respuesta = Paginas::enlacesPaginasModelo($enlaces);
			include($respuesta);	
		}

		/**
		 * [obtenerEnfoqueControlador obtiene los valores de la tabla ENFOQUE]
		 * @return [object] $respuesta [resultados de la consulta o código de error]
		 */
		public function obtenerEnfoqueControlador(){
			$respuesta = Modelo::obtenerEnfoqueModelo('ENFOQUE');
			return $respuesta;
		}

		/**
		 * [obtenerNivelControlador obtiene los valores de la tabla NIVEL]
		 * @return [object] $respuesta [resultados de la consulta o código de error]
		 */
		public function obtenerNivelControlador(){
			$respuesta = Modelo::obtenerNivelModelo('NIVEL');
			return $respuesta;
		}

		/**
		 * [obtenerTodosEjerciciosControlador obtiene todos los ejercicios en la base de datos]
		 * @return [object] $respuesta [resultados de la consulta o código de error]
		 */
		public function obtenerTodosEjerciciosControlador(){
			$respuesta = Modelo::obtenerTodosEjerciciosModelo('EJERCICIO');
			return $respuesta;
		}

	}


?>